package com.united.software.mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

import com.united.software.dto.TemperatureDto;
import com.united.software.model.Temperature;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface TemperatureMapper {

  Temperature TemperatureDtoToTemperature(TemperatureDto temperatureDto);

  TemperatureDto TemperatureToTemperatureDto(Temperature temperature);




}

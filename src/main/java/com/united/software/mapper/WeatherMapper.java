package com.united.software.mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

import com.united.software.dto.WeatherDto;
import com.united.software.model.Weather;
import java.util.Collection;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface WeatherMapper {

  Weather weatherDtoToWeather(WeatherDto weatherDto);

  List<WeatherDto> weathersToWeatherDtos(Collection<Weather> weathers);
  WeatherDto weatherToWeatherDto(Weather weather);




}

package com.united.software.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;


@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class WeatherDto {
  LocalDate date;
  BigDecimal lat;
  BigDecimal lon;
  String city;
  String state;
  List<TemperatureDto> temperatures;
}

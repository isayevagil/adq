package com.united.software.service.impl;

import com.united.software.dto.WeatherDto;
import com.united.software.mapper.WeatherMapper;
import com.united.software.model.Weather;
import com.united.software.repository.WeatherRepository;
import com.united.software.service.WeatherService;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class WeatherServiceImpl implements WeatherService {

  private final WeatherRepository weatherRepository;
  private final WeatherMapper weatherMapper;

  @Override
  public List<WeatherDto> findAll() {
    return weatherMapper.weathersToWeatherDtos(weatherRepository.findAll());
  }

  @Override
  public List<WeatherDto> findAllByCityIsIn(List<String> cities) {
    return weatherMapper.weathersToWeatherDtos(weatherRepository.findAllByCityIsIn(cities));
  }

  @Override
  public List<WeatherDto> findAllByCitySortBy(List<String> cities) {
    return weatherMapper.weathersToWeatherDtos(weatherRepository.findAllByCitySortBy(cities));
  }

  @Override
  public List<WeatherDto> findAllByDate(String date) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    return weatherMapper
        .weathersToWeatherDtos(weatherRepository.findAllByDate(LocalDate.parse(date, formatter)));
  }

  @Override
  public WeatherDto findById(long id) {
    return weatherMapper.weatherToWeatherDto(weatherRepository.findById(id).get());
  }

  @Override
  @Transactional
  public WeatherDto save(WeatherDto weatherDto) {
    Weather weather = weatherMapper.weatherDtoToWeather(weatherDto);
    weather
        .getTemperatures()
        .forEach(temp -> temp.setWeather(weather));
    weatherRepository.save(weather);
    return weatherMapper.weatherToWeatherDto(weather);
  }
}

package com.united.software.service;

import com.united.software.dto.WeatherDto;
import java.util.List;

public interface WeatherService {

  List<WeatherDto> findAll();

  List<WeatherDto> findAllByCityIsIn(List<String> cities);


  List<WeatherDto> findAllByCitySortBy(List<String> cities);
  List<WeatherDto> findAllByDate(String date);


  WeatherDto findById(long id);

  WeatherDto save(WeatherDto weatherDto);
}

package com.united.software.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Builder
@Table(name = "weather")
@NamedEntityGraph
    (name = "Weather.temperatures",
        attributeNodes = @NamedAttributeNode("temperatures"))

public class Weather {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id;
  LocalDate date;
  BigDecimal lat;
  BigDecimal lon;
  String city;
  String state;
  @OneToMany(mappedBy = "weather", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @Fetch(FetchMode.JOIN)
  List<Temperature> temperatures;

}

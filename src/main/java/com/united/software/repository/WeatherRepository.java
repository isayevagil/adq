package com.united.software.repository;

import com.united.software.model.Weather;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface WeatherRepository extends JpaRepository<Weather, Long> {

  @Query("select w from Weather  w join fetch w.temperatures t")
  List<Weather> findAll();

  @Query("select w from Weather  w join fetch w.temperatures t")
  List<Weather> findAllByCityIsIn(List<String> cities);


  Optional<Weather> findById(long id);

  @Query("select w from Weather  w join fetch w.temperatures t")
  List<Weather> findAllByDate(LocalDate date);

  @Query(value = "select  w from Weather  w where   w.city in :cities order by w.date desc")
  List<Weather> findAllByCitySortBy(List<String> cities);

}

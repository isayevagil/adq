package com.united.software;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdqApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdqApplication.class, args);
	}

}

package com.united.software.controller;

import com.united.software.dto.WeatherDto;
import com.united.software.service.WeatherService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/weather")
@RequiredArgsConstructor
public class WeatherController {
  private final WeatherService weatherService;

  @PostMapping(value = "/")
  public WeatherDto save(@RequestBody @Validated WeatherDto weatherDto) {
    return weatherService.save(weatherDto);
  }

  @GetMapping(value = "/")
  public List<WeatherDto> findAll() {
    return weatherService.findAll();
  }

  @GetMapping(value = "/city")
  public List<WeatherDto> findAllByCities(@RequestParam("city") List<String> cities) {
    return weatherService.findAllByCityIsIn(cities);
  }

  @GetMapping(value = "/date")
  public List<WeatherDto> findAllByDate(@RequestParam("date") String date) {
    return weatherService.findAllByDate(date);
  }

  @GetMapping(value = "/id/{id}")
  public WeatherDto findWeatherById(@PathVariable long id) {
    return weatherService.findById(id);
  }

  @GetMapping(value = "/city/sort")
  public List<WeatherDto> findAllByCitiesSortByDate(
      @RequestParam("city") List<String> cities) {
    return weatherService.findAllByCitySortBy(cities);
  }

}

package com.united.software.mapper;

import com.united.software.dto.TemperatureDto;
import com.united.software.model.Temperature;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-10-22T00:42:53+0400",
    comments = "version: 1.5.3.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-7.5.1.jar, environment: Java 17.0.3 (Amazon.com Inc.)"
)
@Component
public class TemperatureMapperImpl implements TemperatureMapper {

    @Override
    public Temperature TemperatureDtoToTemperature(TemperatureDto temperatureDto) {
        if ( temperatureDto == null ) {
            return null;
        }

        Temperature.TemperatureBuilder temperature = Temperature.builder();

        temperature.value( temperatureDto.getValue() );

        return temperature.build();
    }

    @Override
    public TemperatureDto TemperatureToTemperatureDto(Temperature temperature) {
        if ( temperature == null ) {
            return null;
        }

        TemperatureDto.TemperatureDtoBuilder temperatureDto = TemperatureDto.builder();

        temperatureDto.value( temperature.getValue() );

        return temperatureDto.build();
    }
}

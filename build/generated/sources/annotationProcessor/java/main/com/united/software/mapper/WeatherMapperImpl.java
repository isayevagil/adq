package com.united.software.mapper;

import com.united.software.dto.TemperatureDto;
import com.united.software.dto.WeatherDto;
import com.united.software.model.Temperature;
import com.united.software.model.Weather;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-10-22T00:42:53+0400",
    comments = "version: 1.5.3.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-7.5.1.jar, environment: Java 17.0.3 (Amazon.com Inc.)"
)
@Component
public class WeatherMapperImpl implements WeatherMapper {

    @Override
    public Weather weatherDtoToWeather(WeatherDto weatherDto) {
        if ( weatherDto == null ) {
            return null;
        }

        Weather.WeatherBuilder weather = Weather.builder();

        weather.date( weatherDto.getDate() );
        weather.lat( weatherDto.getLat() );
        weather.lon( weatherDto.getLon() );
        weather.city( weatherDto.getCity() );
        weather.state( weatherDto.getState() );
        weather.temperatures( temperatureDtoListToTemperatureList( weatherDto.getTemperatures() ) );

        return weather.build();
    }

    @Override
    public List<WeatherDto> weathersToWeatherDtos(Collection<Weather> weathers) {
        if ( weathers == null ) {
            return null;
        }

        List<WeatherDto> list = new ArrayList<WeatherDto>( weathers.size() );
        for ( Weather weather : weathers ) {
            list.add( weatherToWeatherDto( weather ) );
        }

        return list;
    }

    @Override
    public WeatherDto weatherToWeatherDto(Weather weather) {
        if ( weather == null ) {
            return null;
        }

        WeatherDto.WeatherDtoBuilder weatherDto = WeatherDto.builder();

        weatherDto.date( weather.getDate() );
        weatherDto.lat( weather.getLat() );
        weatherDto.lon( weather.getLon() );
        weatherDto.city( weather.getCity() );
        weatherDto.state( weather.getState() );
        weatherDto.temperatures( temperatureListToTemperatureDtoList( weather.getTemperatures() ) );

        return weatherDto.build();
    }

    protected Temperature temperatureDtoToTemperature(TemperatureDto temperatureDto) {
        if ( temperatureDto == null ) {
            return null;
        }

        Temperature.TemperatureBuilder temperature = Temperature.builder();

        temperature.value( temperatureDto.getValue() );

        return temperature.build();
    }

    protected List<Temperature> temperatureDtoListToTemperatureList(List<TemperatureDto> list) {
        if ( list == null ) {
            return null;
        }

        List<Temperature> list1 = new ArrayList<Temperature>( list.size() );
        for ( TemperatureDto temperatureDto : list ) {
            list1.add( temperatureDtoToTemperature( temperatureDto ) );
        }

        return list1;
    }

    protected TemperatureDto temperatureToTemperatureDto(Temperature temperature) {
        if ( temperature == null ) {
            return null;
        }

        TemperatureDto.TemperatureDtoBuilder temperatureDto = TemperatureDto.builder();

        temperatureDto.value( temperature.getValue() );

        return temperatureDto.build();
    }

    protected List<TemperatureDto> temperatureListToTemperatureDtoList(List<Temperature> list) {
        if ( list == null ) {
            return null;
        }

        List<TemperatureDto> list1 = new ArrayList<TemperatureDto>( list.size() );
        for ( Temperature temperature : list ) {
            list1.add( temperatureToTemperatureDto( temperature ) );
        }

        return list1;
    }
}

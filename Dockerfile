FROM openjdk:15-alpine
RUN echo "hello world"
COPY ./build/libs/adq-0.1.jar /app/
WORKDIR /app/
#  ---- build
ENTRYPOINT ["java"]
CMD ["-jar", "/app/adq-0.1.jar"]

